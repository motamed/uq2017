function r=Matern_cov(dist,sigma2,kappa,nu)
% matern_covariance  calculates Mat�rn covariances
% r=matern_covariance(dist,sigma2,kappa,nu)
%
% r = matrix of covariances, calculated for  the distances in  dist
%     r has the same shape as dist
% sigma2 = variance
% kappa = 1/Lc =reciprocal of the correlation length 
% nu = the Mat�rn covariance parameters


absnu = abs(nu);
absk = abs(kappa);
dpos = (dist>0);
r = zeros(size(dist));
r(~dpos) = sigma2;


[dunique,I,J] = unique(dist(dpos));
dunique = dist(dpos);
dunique = dunique(I);

B = log(besselk(absnu,absk*dunique));
B = exp(log(sigma2)-gammaln(absnu)-(absnu-1)*log(2) + ...
    absnu.*log(absk*dunique) + B);
r(dpos) = B(J);
