
%KL expansion of a 2D Gaussian random field on a retangular mesh
%Written by M. Motamed, UT Austin, July 2011  
%Modified by M. Motamed, UNM, September 2016
%~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
clear all; clc

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%Part 1) Input data
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
Lx1=20; Lx2=10;
N=40*1; %no. of elements in x1-direction
M=20*1; %no. of elements in x2-direction
x1=linspace(0,Lx1,N+1);
x2=linspace(0,Lx2,M+1);
[X1,X2]=meshgrid(x1,x2); %this will generate a grid on the spational domain
                         
dx1=Lx1/N; %spatial grid length in x-direction
dx2=Lx2/M; %spatial grid length in y-direction

mu=1;      %mean of the field
sigma=1.5; %standard deviation of the field 

Lc=3;      %correlation length of the field 
           %(you may need to change Lc)

           
           
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%Part 2) To compute the eigenpairs
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%build the square elements
el_row1=[];
for i=1:M
    el_row1=[el_row1 ((i-1)*(N+1)+1:i*(N+1)-1)];
end
elements=[el_row1;el_row1+1;el_row1+N+2;el_row1+N+1];


X1t=X1'; X2t=X2';
coordi=[X1t(:)';X2t(:)']; 

NE = size(elements,2); % number of elements

centroid=zeros(2,NE);
for i=1:NE
   centroid(:,i)=(1/4)*(coordi(:,elements(1,i))+coordi(:,elements(2,i))+coordi(:,elements(3,i))+coordi(:,elements(4,i)));
end

%construct the covariance matrix
CovMtr=zeros(NE,NE);
for i=1:NE
   Point=centroid(:,i);     
   rc=sqrt(sum((centroid-repmat(Point,1,NE)).^2));
   
   %(a) exponential covariance
   %CovMtr(i,:)=exp((-1/Lc)*rc);
   
   %(b) Matern covariance
   %nu=1; %(you may need to change nu)
   %CovMtr(i,:)=Matern_cov(rc,1,1/Lc,nu);
   
   %(c) squared exponential (Gaussian) covariance
   CovMtr(i,:)=exp((-0.5/(Lc^2))*(rc.^2));
end

areas=dx1*dx2*ones(1,NE);
A = diag(areas);
CovMtr = CovMtr*A;

%compute the eigenpairs 
[eigenVec, eigenVal]=eig(CovMtr);
eigenValRow = diag(eigenVal);
[eigenValSorted, Index] = sort(eigenValRow,'descend');
eigenValSorted = diag(abs(eigenValSorted));

%normalize the eigenVec
temp = sqrt(areas*(eigenVec.^2));
for i=1:NE
   eigenVec_normalized(i,:) = eigenVec(i,:)./temp;
end
eigenVecSorted = eigenVec_normalized(:,Index);



%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Part 3) To plot the eigenvalues
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
EIGS=diag(eigenValSorted);
figure
set(gca,'fontsize', 20);
loglog(EIGS,'x-')
xlabel('i')
ylabel('\lambda_i')


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Part 4) To check if we have enough KL terms to retain enough variance
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%We need to check: sum(EIGS)/(sum(areas)) == sigma^2 (here sigma=1)
NKL=0;
VAL=0;
VAR_TOL=0.90; %(you may need to change this tolerance)
while VAL<VAR_TOL 
    NKL=NKL+1;
    VAL=sum(EIGS(1:NKL))/(sum(areas));
end
disp(['Need NKL = ' num2str(NKL) ' KL terms to retain ' num2str(100*VAR_TOL) ' percent of variance'])



%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Part 5) To generate a sample/realization of the field
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
NKL_inf=length(eigenValSorted); %We keep all terms in KL expansion
W=randn(NKL_inf,1); %this will generate a set of NKL_inf normal random variables
a=mu+sigma*eigenVecSorted(:,1:NKL_inf)*sqrt(eigenValSorted(1:NKL_inf,1:NKL_inf))*W(1:NKL_inf);
figure
set(gca,'fontsize', 20);
surf(X1(1:end-1,1:end-1)+dx1/2,X2(1:end-1,1:end-1)+dx2/2,reshape(a,N,M)')
shading interp
view(2)
box
xlabel('x_1')
ylabel('x_2')




