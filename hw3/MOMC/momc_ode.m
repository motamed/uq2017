function [sum1,totcost] = momc_ode(l,M,h,q0,T,beta)

% Witten by M. Motamed at UNM. 2016

totcost = 0;

NT=round(T/h); % fixed no. of time steps (fixed mesh)

% ORDER OF THE METHOD
quf = q0 + beta*l;

if l>0
    quc = quf - beta;
end

sum1 = zeros(1,2);

for N1 = 1:M
    U  = rand(1,1);
    [uf,cost] = ode_taylor(quf,T,NT,U);
    totcost = totcost+cost;
    Qf = uf(1);   % output QoI for fine level
    if l==0
        Qc = 0;
    else
        [uc,cost] = ode_taylor(quc,T,NT,U);
        totcost = totcost+cost;
        Qc = uc(1);     % output QoI for coarse level
    end
    sum1(1) = sum1(1) + (Qf-Qc);
    sum1(2) = sum1(2) + (Qf-Qc).^2;
end
