% HW 3 - UQ course (Fall 2018 at Uppsala University)
% MOMC Performance for a linear ODE system 
% Deterministic solver is a q-th order accurate Taylor's method
% Written by M. Motamed at UNM in 2015 (modified in Sep. 2016)
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%Part 1 - Input parameters

Eps=(20:-1:1)*(1e-4);        %set of tolerances 

T=1;                         %final time
EQ_ex=(cos(T)-cos(2*T))/T;   %the exact value of mean of Q for this problem

momc_fn='momc_ode'; 
M0=100;                      %initial guess for the number of samples

h=1/4;              %fixed step size
q0=2;               %initial order of accuracy of deterministic solver
beta=2;             %order of accuracy increment parameter
kappa1=1;           %related to convergence rate of weak error 
kappa2=2;           %related to convergence rate of strong error
gamma2=1;           %it appears in the work formula Wm = q^(gamma2)
theta=0.9;          %splitting parameter
C_alpha=4;          %constant in statistical error, given a failure probability
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%Part 2 - Computation

rng('default');             %it resets random number generator

N_eps=length(Eps);
MOMC_error=zeros(1,N_eps);
MOMC_cost=zeros(1,N_eps);

for i = 1:N_eps
    disp(i)
    eps = Eps(i);
    [EQ, Nl,cost] = momc(M0,eps,momc_fn,q0,kappa1,kappa2,h,beta,gamma2,theta,C_alpha,T);
    
    MOMC_error(i)=abs(EQ_ex-EQ);
    MOMC_cost(i) = cost;
end
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%Part 3 - Plots

figure(1); 
set(gca,'fontsize', 20);
loglog(Eps,Eps,'b-')
hold on
loglog(Eps,MOMC_error,'rx')
xlabel('tolerance (\epsilon)'); ylabel('MOMC ERROR');

figure(2); 
set(gca,'fontsize', 20);
loglog(Eps,MOMC_cost,'rx-')
hold on
xlabel('tolerance (\epsilon)'); ylabel('MOMC COST');

