function [EQ, Nl,cost] = momc(M0,eps,momc_l,q0,kappa1,kappa2,h,beta,gamma2,theta,C_alpha,T)

% Witten by M. Motamed at UNM. 2016

t_start=cputime;

L             = 2;
Nl(1:3)       = 0;
suml(1:2,1:3) = 0;
dNl(1:3)      = M0;

while sum(dNl) > 0

% update sample sums

    for l=0:L
      if dNl(l+1) > 0
        sums = feval(momc_l,l,dNl(l+1),h,q0,T,beta);
        Nl(l+1)     = Nl(l+1) + dNl(l+1);
        suml(1,l+1) = suml(1,l+1) + sums(1);
        suml(2,l+1) = suml(2,l+1) + sums(2);
      end
    end

% compute variances for levels l=0:L (fromula (6) in lecture notes)

    mul = abs(suml(1,:)./Nl);
    Vl = max(0, suml(2,:)./Nl - mul.^2);

% update optimal number of samples (fromula (4) in lecture notes)

    Wl=(q0+beta*(0:L)).^gamma2;
    Ns  = ceil(sqrt(Vl./Wl) * sum(sqrt(Vl.*Wl)) / (theta*eps^2/C_alpha));
    dNl = max(0, Ns-Nl);

% if (almost) converged, estimate remaining error and decide 
% whether a new level is required

    if sum( dNl > 0.01*Nl ) == 0
      range = -2:0;
      KB=kappa1*beta;
      rem=((h^KB)/(1-h^KB))*max(mul(L+1+range).*h.^(-KB*range)); %formula (5)
      if rem > (1-theta)*eps
        L=L+1;
        Vl(L+1) = (h^(kappa2*beta))*Vl(L);    %formula (7) in lecture notes
        Nl(L+1) = 0;
        suml(1:2,L+1) = 0;

        Wl=(q0+beta*(0:L)).^gamma2;
        Ns  = ceil(sqrt(Vl./Wl) * sum(sqrt(Vl.*Wl)) / (theta*eps^2/C_alpha));
        dNl = max(0, Ns-Nl);
      end
    end
end

% finally, evaluate multilevel estimator
EQ = sum(suml(1,:)./Nl);

cost = cputime - t_start; 
