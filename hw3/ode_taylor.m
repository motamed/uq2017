function [U,cost] = ode_taylor(q,T,NT,Ym)

% Witten by M. Motamed at UNM. 2016

%Talor's method for solving the ODE system U' = A U
%======================================================
% q  = order of accuracy
% T  = final time; 
% NT = number of sub-intervals (or elements)
% Ym = a realization of the random variable in matrix A
%======================================================

h = T/NT;                      % time step (or mesh size)
U = [0;1];                     % initial solution of the ODE
A = [0 1+Ym;-(1+Ym) 0];        % coefficient matrix

t_start = cputime;

for it = 1:NT
    UT = U;
    df = h;
    for k = 1:q
        UT = A*UT;
        U = U + df*UT;
        df = df*h/(k+1);
    end
end
 
cost = cputime - t_start; 

