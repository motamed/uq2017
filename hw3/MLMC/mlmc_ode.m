function [sum1,totcost] = mlmc_ode(l,M,q,T,h0,beta)

% Witten by M. Motamed at UNM. 2016

totcost = 0;
 
nf=(T/h0)*(beta^l);

if l>0
    nc = nf/beta;
end

sum1 = zeros(1,2);

% ORDER OF THE METHOD 
quf = q;
quc = q;

for N1 = 1:M
    U  = rand(1,1);
    [uf,cost] = ode_taylor(quf,T,nf,U);
    totcost = totcost+cost;
    Qf = uf(1);   % output QoI for fine mesh
    if l==0
        Qc = 0;
    else
        [uc,cost] = ode_taylor(quc,T,nc,U);
        totcost = totcost+cost;
        Qc = uc(1);     % output QoI for coarse mesh
    end
    sum1(1) = sum1(1) + (Qf-Qc);
    sum1(2) = sum1(2) + (Qf-Qc).^2;
end
