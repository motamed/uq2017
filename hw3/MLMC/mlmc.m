function [EQ, Nl,cost] = mlmc(M0,eps,mlmc_l,q,q1,q2,h0,beta,gamma,theta,C_alpha,T)

% Witten by M. Motamed at UNM. 2016

t_start=cputime;

L             = 2;
Nl(1:3)       = 0;
suml(1:2,1:3) = 0;
dNl(1:3)      = M0;

while sum(dNl) > 0

% update sample sums

    for l=0:L
      if dNl(l+1) > 0
        sums = feval(mlmc_l,l,dNl(l+1),q,T,h0,beta);
        Nl(l+1)     = Nl(l+1) + dNl(l+1);
        suml(1,l+1) = suml(1,l+1) + sums(1);
        suml(2,l+1) = suml(2,l+1) + sums(2);
      end
    end

% compute variances for levels l=0:L (fromula (6) in lecture notes)

    mul = abs(suml(1,:)./Nl);
    Vl = max(0, suml(2,:)./Nl - mul.^2);

% update optimal number of samples (fromula (4) in lecture notes)

    Wl  = beta.^(gamma*(0:L));
    Ns  = ceil(sqrt(Vl./Wl) * sum(sqrt(Vl.*Wl)) / (theta*eps^2/C_alpha));
    dNl = max(0, Ns-Nl);

% if (almost) converged, estimate remaining error and decide 
% whether a new level is required

    if sum( dNl > 0.01*Nl ) == 0 %otherwise the updated no. of samples is not negligable
                                 %and hence we need to update the summs again
      range = -2:0;
      rem = max(mul(L+1+range).*beta.^(q1*range))/(beta^q1 - 1); %formula (5)
      if rem > (1-theta)*eps
        L=L+1;
        Vl(L+1) = Vl(L) / beta^q2;   %formula (7) in lecture notes
        Nl(L+1) = 0;
        suml(1:2,L+1) = 0;

        Wl  = beta.^(gamma*(0:L));
        Ns  = ceil(sqrt(Vl./Wl) * sum(sqrt(Vl.*Wl)) / (theta*eps^2/C_alpha));
        dNl = max(0, Ns-Nl);
      end
    end 
end

% finally, evaluate multilevel estimator
EQ = sum(suml(1,:)./Nl);

cost = cputime - t_start; 