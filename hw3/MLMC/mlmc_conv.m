% HW 3 - UQ course (Fall 2018 at Uppsala University)
% MLMC Performance for a linear ODE system 
% Deterministic solver is a q-th order accurate Taylor's method
% Written by M. Motamed at UNM in 2015 (modified in Sep. 2016)
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%Part 1 - Input parameters

Eps=(20:-1:1)*(1e-4);            %set of tolerances 

T=1;                             %final time
EQ_ex=(cos(T)-cos(2*T))/T;       %the exact value of mean of Q for this problem

mlmc_fn='mlmc_ode'; 
M0=100;             %initial guess for the number of samples

q=2;                %fixed order of accuracy of deterministic solver
q1 = q;             %convergence rate of weak error 
q2 = 2*q;           %convergence rate of strong error
h0 = 0.5;           %initial step size
beta=2;             %mesh refinement parameter
gamma = 1;          %it appears in the work formula Wm = h^(-gamma)
theta=0.9;          %splitting parameter
C_alpha=4;        %constant in statistical error, given a failure probability
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%Part 2 - Computation

rng('default');    % reset random number generator

N_eps = length(Eps);  
MLMC_error=zeros(1,N_eps);
MLMC_cost=zeros(1,N_eps);

for i = 1:N_eps
    disp(i)
    eps = Eps(i);
    [EQ, Nl,cost] = mlmc(M0,eps,mlmc_fn,q,q1,q2,h0,beta,gamma,theta,C_alpha,T);
    
    MLMC_error(i)=abs(EQ_ex-EQ);
    MLMC_cost(i) = cost;
end
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%Part 3- Plots

figure(1);
set(gca,'fontsize', 20);
loglog(Eps,Eps,'b-')
hold on
loglog(Eps,MLMC_error,'gx')
xlabel('tolerance (\epsilon)'); ylabel('MLMC ERROR');

figure(2);
set(gca,'fontsize', 20);
loglog(Eps,MLMC_cost,'gx-')
hold on
xlabel('tolerance (\epsilon)'); ylabel('MLMC Cost');

