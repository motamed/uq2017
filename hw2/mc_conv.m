% HW 2 - UQ course (Fall 2018 at Uppsala U.)
% MC Performance for a linear ODE system
% Deterministic solver is a q-th order accurate Taylor's method
% Written by M. Motamed at UNM in 2015 (modified in Oct. 2018)
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%Part 1 - Input parameters

Eps=(10:-1:1)*(1e-3);            %set of tolerances
                                 %(you may need to change this)

T=1;                             %final time
EQ_ex=(cos(T)-cos(2*T))/T;       %the exact value of mean of Q for this problem

q=2;      %fixed order of accuracy q of deterministic solver 
          %(you may need to change this)

gamma=1;  %it appears in the work formula Wm = q . h^(- gamma)
          %For our ODE problem it is always 1, but for PDEs it may be more than 1
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%Part 2 - Computation

rng('default');            %reset random number generator

MC_cost=zeros(1,length(Eps));
MC_error=zeros(1,length(Eps));

for i = 1:length(Eps)
    disp(i)
    eps = Eps(i);
    [EQ,VQ,M,cost] = mc(eps,q,gamma,T);
    
    MC_error(i)=abs(EQ_ex-EQ);
    MC_cost(i) = cost;  
end
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%Part 3 - Plots

figure(1); 
set(gca,'fontsize', 20);
loglog(Eps,Eps,'b-')
hold on
loglog(Eps,MC_error,'kx')
xlabel('tolerance (\epsilon)'); ylabel('MC ERROR');
  
figure(2);
loglog(Eps,MC_cost,'kx-')
hold on
xlabel('tolerance (\epsilon)'); ylabel('MC COST');
