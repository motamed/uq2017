function [EQ,VQ,M,cost] = mc(eps,q,gamma,T)

% Witten by M. Motamed at UNM. 2016

%(1) set optimal number of samples M
VQ=.002; %variance (we know this is approximately correct for this problem)
M=ceil((((gamma+2*q)/(2*q)))^2*VQ*eps^(-2)); 

%(2) set optimal time step h 
h=(eps/(1+2*q/gamma))^(1/q);
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
t_start=cputime;

NT=ceil(T/h);    %number of sub-intervals (or elements)  
 
sumQ = zeros(1,2);
YM=rand(M,1);

for j = 1:M
    Ym= YM(j);
    U = ode_taylor(q,T,NT,Ym);      % ODE solution U at final time T
    Q = U(1);                       % output QoI 
    
    sumQ(1) = sumQ(1) + Q;
    sumQ(2) = sumQ(2) + Q.^2;
end

%%%%%%%
EQ = sumQ(1)/M;                    % mean of Q
VQ = max(0, sumQ(2)/M - EQ^2);     % variance of Q
cost = cputime - t_start; 
